class CreateRoles < ActiveRecord::Migration[6.0]
  def change
    create_table :roles do |t|
      t.string :name
      t.string :permissions
      t.references :organization, null: false, foreign_key: true

      t.timestamps
    end

    create_join_table :roles, :users
  end
end

class CreatePermissions < ActiveRecord::Migration[6.0]
  def change
    create_table :permissions do |t|
      t.string :name
      t.string :slug

      t.timestamps
    end

    create_join_table :permissions, :users
    create_join_table :permissions, :roles
  end
end

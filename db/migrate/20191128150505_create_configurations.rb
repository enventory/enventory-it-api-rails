class CreateConfigurations < ActiveRecord::Migration[6.0]
  def change
    create_table :configurations do |t|
      t.string :key
      t.string :value
      t.boolean :encrypted

      t.timestamps
    end
  end
end

class CreateLocations < ActiveRecord::Migration[6.0]
  def change
    create_table :locations do |t|
      t.string :name
      t.references :location, null: true, foreign_key: true
      t.string :distinguished_name
      t.string :contact
      t.references :organization, null: true, foreign_key: true

      t.timestamps
    end
  end
end

class CreateDepartments < ActiveRecord::Migration[6.0]
  def change
    create_table :departments do |t|
      t.string :name
      t.references :supervisor, null: false, foreign_key: { to_table: :users}
      t.references :organization, null: true, foreign_key: true
      t.timestamps
    end
  end
end

class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :username
      t.string :first_name
      t.string :last_name
      t.string :language
      t.string :password
      t.string :temp_pass

      t.timestamps
    end

    create_join_table :users, :organizations
  end
end

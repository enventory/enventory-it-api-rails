source 'http://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.5'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.1'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 4.1'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0'
gem 'redis-rails'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# We are going to use nginx as a frontend and proxy any api requests to rails, so we won't need this
gem 'rack-cors'

# Authentication libraries
gem 'devise-jwt'
# gem 'omniauth', '~> 1.9'
# gem 'omniauth-auth0', '~> 2.2.0'
# gem 'omniauth-azure-oauth2', '~> 0.0.9'
# gem 'omniauth-cas3', '~> 1.1.4'
# gem 'omniauth-facebook', '~> 5.0.0'
# gem 'omniauth-github', '~> 1.3'
# gem 'omniauth-gitlab', '~> 2.0'
# gem 'omniauth-google-oauth2', '~> 0.6.0'
# gem 'omniauth-kerberos', '~> 0.3.0', group: :kerberos
# gem 'omniauth-oauth2-generic', '~> 0.2.2'
# gem 'omniauth-saml', '~> 1.10'
# gem 'omniauth-shibboleth', '~> 1.3.0'
# gem 'omniauth-twitter', '~> 1.4'
# gem 'omniauth_openid_connect', '~> 0.3.3'
# gem 'gssapi', group: :kerberos

# 2FA
# gem 'u2f'

# Files
gem 'carrierwave', '~> 2.0'

# Certificates
gem 'acme-client'

# Software versions
gem 'version_sorter'

# Search
gem 'elasticsearch-model', '~> 7.0'
gem 'elasticsearch-rails', '~> 7.0'
gem 'elasticsearch-api', '~> 7.4'

# SSH for scanning, remoting, etc.
gem 'net-ssh'
gem 'sshkey'

# Countries (origin, languages...)
gem 'countries'

# Markdown
# Not needed on server side?

# Sidekiq?

# Integrations with communication platforms
gem 'slack-notifier'
gem 'wrike3'
gem 'microsoft_graph'
gem 'mattermost-ruby'


group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'rubocop'
  gem 'rubocop-rspec'
  gem 'rubocop-performance'
  gem 'ffaker'
  gem 'rspec-rails'
end

group :development do
  gem 'danger'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

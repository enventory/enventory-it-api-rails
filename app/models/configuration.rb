class Configuration < ApplicationRecord
  # Validation
  validates :key, uniqueness: true, presence: true
  validates :value, presence: true
end

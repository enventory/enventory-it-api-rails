class User < ApplicationRecord
  has_secure_password
  has_and_belongs_to_many :organizations
  has_and_belongs_to_many :roles
  has_many :locations
  has_many :departments
  has_and_belongs_to_many :permissions, through: :roles

  devise :database_authenticable, :jwt_authenticable, jwt_revocation_strategy: JWTBlacklist

  # Validation
  validates :email,
            presence: true,
            uniqueness: true,
            format: { with: URI::MailTo::EMAIL_REGEXP}
  validates :username,
            presence: true,
            uniqueness: true
  validates :password,
            length: { minimum: 6 },
            if: -> { new_record? || !password.nil? }
end
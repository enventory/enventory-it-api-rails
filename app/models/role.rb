class Role < ApplicationRecord
  belongs_to :organization
  has_and_belongs_to_many :users
  has_and_belongs_to_many :permissions

  # after_commit :destroy_users_other_roles_if_admin, on: [:create, :update]

  # def destroy_users_other_roles_if_client_admin
  #   users_roles
  # end
end

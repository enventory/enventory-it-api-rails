class Organization < ApplicationRecord
  has_and_belongs_to_many :users
  has_many :roles
  has_many :departments
  has_many :users
  has_many :locations
end

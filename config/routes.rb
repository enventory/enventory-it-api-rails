Rails.application.routes.draw do
  get 'computers/getall'
  get 'computers/get'
  get 'computers/search'
  get 'computers/new'
  get 'computers/update'
  get 'computers/decommission'
  get 'computers/clean'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  #
  # Devise (Authorization)
  devise_for :users,
             path: '',
             path_names: {
                 sign_in: 'login',
                 sign_out: 'logout',
                 registration: 'signup'
             },
             controllers: {
                 sessions: 'sessions',
                 registrations: 'registrations'
             }
end
